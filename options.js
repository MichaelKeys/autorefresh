function save_options() {
    var hostnameInput = document.getElementById('hostnameInput').value;
    var refreshPeriodInput = document.getElementById('refreshPeriodInput').value;
    chrome.storage.sync.set({
        hostname: hostnameInput,
        refreshPeriod: refreshPeriodInput
    }, function() {
        var status = document.getElementById('saveStatus');
        $(status).show();
        $('#hostnameInputStatus').hide();
        $('#refreshPeriodInputStatus').hide();
        status.textContent = "Options Saved";
        setTimeout(function () {
            status.textContent = '';
            $(status).hide();
        }, 750);
    });
    console.log("AutoRefresh+ Hostname updated to: ", hostnameInput);
}

function validate_options() {
    var hostnameInput = document.getElementById('hostnameInput').value;
    var refreshPeriodInput = document.getElementById('refreshPeriodInput').value;
    var hostnameInputStatus = document.getElementById('hostnameInputStatus');
    var refreshPeriodInputStatus = document.getElementById('refreshPeriodInputStatus');
    if (hostnameInput == '') {
        $(hostnameInputStatus).show();
        hostnameInputStatus.textContent = "Hostname field must not be empty.";
        setTimeout(function () {
            hostnameInputStatus.textContent = '';
            $(hostnameInputStatus).hide();
        }, 5000);
    }
    else if (refreshPeriodInput == '') {
        $(refreshPeriodInputStatus).show();
        refreshPeriodInputStatus.textContent = "Refresh Period field must not be empty.";
        setTimeout(function () {
            $(refreshPeriodInputStatus).hide();
            refreshPeriodInputStatus.textContent = '';
        }, 5000);
    }
    else {
        save_options();
    }
}

function restore_options() {
    chrome.storage.sync.get({
        hostname: 'mail.google.com',
        refreshPeriod: '10'
    }, function(items) {
        document.getElementById('hostnameInput').value = items.hostname;
        document.getElementById('refreshPeriodInput').value = items.refreshPeriod;
    });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', validate_options);