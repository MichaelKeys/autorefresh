let Refresh = document.getElementById('Refresh');
let Options = document.getElementById('Options');

// This will refresh all tabs that fit the hostname provided by the user
Refresh.onclick = function(element) {
    chrome.storage.sync.get({
        hostname: 'mail.google.com'
    }, function(items) {
        console.log('*://' + items.hostname + '/*');
        if (items.hostname != false) {
            chrome.tabs.query({url: '*://' + items.hostname + '/*'}, function(tabs) {
                tabs.forEach((tab)=>{
                        reloadTab(tab.id);
                    });
            });
        }
    });
}

function reloadTab(id) {
    chrome.tabs.reload(id);
    console.log("AutoRefresh+ Tab Reloaded by pressing Refresh Now: ", id);
}

Options.onclick = function(element) {
    chrome.runtime.openOptionsPage();
}