# AutoRefresh+

Powerful timed webpage refresh extension for Chrome.

### Chrome Web Store
This extension can be added to Google Chrome from the Chrome Web Store below:

 - https://chrome.google.com/webstore/detail/autorefresh%20/jdcmniiepfjinccadkbjliiopialnnmi