chrome.runtime.onInstalled.addListener(function() {
    chrome.storage.sync.set({hostname: ''}, function() {
        console.log("Hostname initialized");
    });
    chrome.storage.sync.set({refreshPeriod: ''}, function() {
        console.log("Refresh Period initialized");
    })
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
        chrome.declarativeContent.onPageChanged.addRules([{
          conditions: [new chrome.declarativeContent.PageStateMatcher({
            pageUrl: {urlContains: '/'},
            })
            ],
              actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});

chrome.storage.sync.get({
    refreshPeriod: ''
}, function(items) {
    if (items.refreshPeriod != false) {
        chrome.alarms.create({
            delayInMinutes: parseInt(items.refreshPeriod),
            periodInMinutes: parseInt(items.refreshPeriod)
        });
    }
});

  
chrome.alarms.onAlarm.addListener(function(alarm) {
    chrome.storage.sync.get({
        hostname: 'mail.google.com'
    }, function(items) {
        console.log('*://' + items.hostname + '/*');
        if (items.hostname != false) {
            chrome.tabs.query({url: '*://' + items.hostname + '/*'}, function(tabs) {
                tabs.forEach((tab)=>{
                        reloadTab(tab.id);
                });
            });
        }
    });
});

function reloadTab(id) {
    chrome.tabs.reload(id);
    console.log("AutoRefresh+ Tab Reloaded", id);
}